import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Service } from 'src/app/servicios/service';
import * as moment from 'moment';

@Component({
  selector: 'app-modal-agenda',
  templateUrl: './modal-agenda.component.html',
  styleUrls: ['./modal-agenda.component.css']
})
export class ModalAgendaComponent implements OnInit {



  firstFormGroup: FormGroup = this._formBuilder.group({
    documento: ['', [Validators.required]],
    fecVisita: ['', [Validators.required]],
    horVisita: ['', [Validators.required]],
    telefono: ['', [Validators.required]],
    usuario: ['', [Validators.required]],

  });


  constructor(private _formBuilder: FormBuilder, public dialogRef: MatDialogRef<ModalAgendaComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any, private service: Service) {


    if (data === null || data === undefined) {
      return;
    }

    this.firstFormGroup = this._formBuilder.group({
      documento: [data.data.cod_sucursal, [Validators.required]],
      fecVisita: [data.data.nombre, [Validators.required]],
      horVisita: [data.data.cod_sucursal, [Validators.required]],
      telefono: [data.data.nombre, [Validators.required]],
      usuario: [data.data.cod_sucursal, [Validators.required]]

    });

  }

  ngOnInit(): void {


  }


  grabarVisita() {

    let param = {
      documento: this.firstFormGroup.value.documento,
      fechaVisita: this.firstFormGroup.value.fecVisita,
      horaVisita: this.firstFormGroup.value.horVisita,
      telefono: this.firstFormGroup.value.telefono,
      usuario: this.firstFormGroup.value.usuario
    }

    this.obtenerAgendas(param);
  }

  private Grabar(param: any) {
    this.service.agregaVisita(param).subscribe((data) => {
      this.dialogRef.close(true);
    }, (err) => {
    });
  }

  private obtenerAgendas(param: any) {
    var existe = false;
    this.service.listaAgendas(param.documento).subscribe((data) => {
      var fecha = null;
      var fechaNew = moment(param.fechaVisita, 'DD/MM/YYYY')
      data.forEach((e: any) => {
        fecha = moment(e.fechaVisita, 'DD/MM/YYYY');
        var hora = moment(e.horaVisita, 'HH:mm');
        var horaNew = moment(param.horaVisita, 'HH:mm');
        if (fecha.isSame(fechaNew)) {
          var duration = moment.duration(horaNew.diff(hora)).asMinutes();
          if (duration < 30) {
            existe = true;
          }
        }
      });

      if (!existe) {
        this.Grabar(param);
      } else {
        console.log('Ya existe agendada a esa hora');
      }
    }, (err) => {
    });
  }
}
