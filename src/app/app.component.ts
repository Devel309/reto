import { Component } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { ModalProductoComponent } from './pages/modal-producto/modal-producto.component';
import { Service } from './servicios/service';
import { Util } from 'src/app/clases/Util';
import { ModalAgendaComponent } from './pages/modal-agenda/modal-agenda.component';
import { ModalUsuarioComponent } from './pages/modal-usuario/modal-usuario.component';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'crud';

  mostrarAgendas: boolean = false;
  mostrarProducto: boolean = false;
  mostrarUsuario: boolean = false;

  displayedColumnsProducto: string[] = ['codigo', 'name', 'precio', 'accion'];
  dataSourceProducto: any[] = [];

  displayedColumnsSucursal: string[] = ['documento', 'fechaVisita', 'horaVisita', 'telefono', 'usuario',];
  dataSourceAgenda: any[] = [];

  displayedColumnsUsuario: string[] = ['codigo', 'name', 'user', 'password', 'sucursal', 'accion'];
  dataSourceUsuario: any[] = [];

  constructor(private router: Router, private _formBuilder: FormBuilder, public dialog: MatDialog,
    private service: Service) { }

  ngOnInit() {

    this.mostrarAgendas  = false;
    this.mostrarProducto  = false;
    this.mostrarUsuario  = false;
  }


  listarProductos() {
    this.mostrarAgendas  = false;
    this.mostrarProducto  = true;
    this.mostrarUsuario  = false;

    /////this.getListarProductos();
  }

  eliminarProducto(row: any) {
    /////this.getEliminarProducto(row.cod_producto);
  }

  listarAgendas() {

    this.mostrarAgendas  = true;
    this.mostrarProducto  = false;
    this.mostrarUsuario  = false;


    this.dataSourceProducto = [];
    this.dataSourceAgenda = [];
    this.obtenerAgendas('71378267');
  }

  listarUsuario() {

    this.mostrarAgendas  = false;
    this.mostrarProducto  = false;
    this.mostrarUsuario  = true;


    this.dataSourceProducto = [];
    this.dataSourceAgenda = [];
    /////this.getListarUsuario();
  }

  eliminarSucursal(row: any) {
    /////this.getEliminarSucursal(row.cod_sucursal);
  }

  eliminarUsuario(row: any) {
    /////this.getEliminarUsuario(row.cod_usuario);
  }

  openModalProducto(row: any) {

    let dialogRef: any;

    let data: any = Util.clonar(row);

    dialogRef = this.dialog.open(ModalProductoComponent, {
      disableClose: false,
      width: '900px',
      height: '400px',
      data: { data }
    });

    this.dialog.afterAllClosed.subscribe(
      () => {
        this.listarProductos();
      }
    );
  }

  openModalSucursal(row: any) {

    let dialogRef: any;

    let data: any = Util.clonar(row);

    dialogRef = this.dialog.open(ModalAgendaComponent, {
      disableClose: false,
      width: '900px',
      height: '400px',
      data: { data }
    });

    this.dialog.afterAllClosed.subscribe(
      () => {
        this.listarAgendas();
      }
    );

  }

  openModalUsuario(row: any) {

    let dialogRef: any;

    let data: any = Util.clonar(row);

    dialogRef = this.dialog.open(ModalUsuarioComponent, {
      disableClose: false,
      width: '900px',
      height: '400px',
      data: { data }
    });

    this.dialog.afterAllClosed.subscribe(
      () => {
        this.listarUsuario();
      }
    );

  }


  openModalGrabarAgenda() {

    let dialogRef: any;

    dialogRef = this.dialog.open(ModalAgendaComponent, {
      disableClose: false,
      width: '900px',
      height: '800px',
      data: undefined
    });


    this.dialog.afterAllClosed.subscribe(
      () => {
        this.listarAgendas();
      }
    );
  }

  openModalGrabarUsuario() {

    let dialogRef: any;

    dialogRef = this.dialog.open(ModalUsuarioComponent, {
      disableClose: false,
      width: '900px',
      height: '400px',
      data: undefined
    });

    this.dialog.afterAllClosed.subscribe(
      () => {
        this.listarUsuario();
      }
    );


  }

  openModalGrabarProducto() {


    let dialogRef: any;



    dialogRef = this.dialog.open(ModalProductoComponent, {
      disableClose: false,
      width: '900px',
      height: '400px',
      data: undefined
    });

    this.dialog.afterAllClosed.subscribe(
      () => {
        this.listarProductos();
      }
    );


  }


  private obtenerAgendas(documento: string) {
    this.service.listaAgendas(documento).subscribe((data) => {
      this.dataSourceAgenda = data;
    }, (err) => {
    });
  }

}
