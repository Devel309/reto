import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';

import { throwError, Observable } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { environment } from 'src/environments/environment';


@Injectable({
  providedIn: 'root'
})
export class Service {

  private readonly AGENDA: string = `${environment.api_base}/agenda/USER`;
  private readonly REGISTRAR: string = `${environment.api_base}/agenda/registrar`;
  private readonly DOCUMENTO: string = `${environment.api_base}/persona`;
  private readonly LISTAR: string = `${environment.api_base}/persona/lista`;

  constructor(private http: HttpClient) { }

  public listaAgendas(codigoUsuario: string): Observable<any> {
    let param = new HttpParams();
    param = param.append('codigoUsuario',codigoUsuario);
    return this.http.get<any>(this.AGENDA, {params: param})
      .pipe(
        catchError(this.handleError)
      );
  }

  public agregaVisita(data: any): Observable<any> {
    return this.http.post<any>(this.REGISTRAR, data)
      .pipe(
        catchError(this.handleError)
      );
  }

  public documentosPersona(documento: string): Observable<any> {
    let param = new HttpParams();
    param = param.append('documento ',documento );
    return this.http.get<any>(this.DOCUMENTO)
      .pipe(
        catchError(this.handleError)
      );
  }

  public personaListar(data: any): Observable<any> {
    return this.http.post<any>(this.LISTAR, data)
      .pipe(
        catchError(this.handleError)
      );
  }

  // Error handling
  handleError(error: any) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    return throwError(errorMessage);
  }
}
